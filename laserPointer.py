########################################
#
# laserPointer.py
#
# Michael Graves, Suhail Desai, Umair Suhail, Anjan Momi. November 2019
#
# This library handles the serial communication to the laser indicator.
# 
# Commands consist of a single character, an optional value, and a semi-colon
#  to mark the end of the command.
#
########################################

import serial

s = None

# toRange((v + Offset) * scale)
rangeServo =    (45, 135)
scaleServo =    [2, 2]
offsetServo =   90

rangeLaser =    (0, 255)

def open(COM):
    global s
    s = serial.Serial(COM, 115200, timeout=1)
    if(s.is_open):
        print("Serial port is open!")

def close():
    s.close()

def send(string):
    #print(string)
    s.write(string)

def toRange(v, lower, upper):
    return min(max(v, lower), upper)

def setLaser(power):
    if(s == None):
        print("ERR: Serial port not setup!")
        return
    power = toRange(power, rangeLaser[0], rangeLaser[1])
    send(b'l%i;' % int(power))
    
def setPitch(pitch):
    if(s == None):
        print("ERR: Serial port not setup!")
        return
    pitch = toRange((pitch * scaleServo[0] + offsetServo) , rangeServo[0], rangeServo[1])
    send(b'p%i;' % int(pitch))

def setRoll(roll):
    if(s == None):
        print("ERR: Serial port not setup!")
        return
    roll = toRange((roll * scaleServo[1] + offsetServo), rangeServo[0], rangeServo[1])
    send(b'r%i;' % int(roll))

def setTarget(pitch, roll):
    setPitch(pitch)
    setRoll(roll)
