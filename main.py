########################################
#
# main.py
#
# Michael Graves, Suhail Desai, Umair Suhail, Anjan Momi. November 2019
#
# This file performs the image processing algorithms and controls the laser indicator.
#
########################################

from imutils.video import VideoStream
from pyquaternion import Quaternion
import time
import numpy as np
import cv2
import imutils
import math

import laserPointer as lp

# Program Parameters
COMPort = "COM10"
targetMarkerName = "DLPFC"
laserPower = 5 #/255
laserUpdatePeriod = 0.1
laserOffset = [8, 0]

# Frame Settings sizes
frameWidth = 960
croppedSize = 600

# Camera settings
cameraIndex = 1
cameraFOV = 60
cameraDegPerPx = 0 # Calculated below FOV/diagPX

# Cap colors/size
greenLower = (35, 86, 70)
greenUpper = (100, 255, 240)
minimumRadius = 50     #Px

# Marker colors/sizes
markerLower = (0,0,0)
markerUpper = (180, 255, 80)
markerMinimumRadius = 5

lineLower = (0,0,0)
lineUpper = (180, 255, 85)

markerDefaultRadius = 10
markerColourCz = (255,255,255) #White
markerColourKnown = (0, 0, 255) # Red
markerColourUnknown = (0, 0, 100) # Dark Red
markerColourExtrapolate = (0, 255, 255) # Yellow

markerToQuaternionFudgeFactor = 2

fCZThreshold = 0.8 #This is the ratio of distances from CZ, FCZ is closer than all other points.
hemiThreshold = 0.5 #This is the threshold for the cross product to determine if a point is on the hemisphere
leftHemiPositive = True #Left = +ve dotProduct (Compared against the inequality (dotProduct >= 0) == leftThreshold)
ofpZThreshold = 0.66 # This is the ratio of the distance to FCZ/distance to (FpZ/Oz)

# Marker locations defined as Quaternions
# https://www.biosemi.com/download/Cap_coords_all.xls
markerLocs = {
    "FpZ":  Quaternion(axis=[1,0,0], angle=math.pi * 0.40),
    "Fz":   Quaternion(axis=[1,0,0], angle=math.pi * 0.20),
    "FCZ":  Quaternion(axis=[1,0,0], angle=math.pi * 0.1),
    "Cz":   Quaternion(),
    "C4":   Quaternion(axis=[0,1,0], angle=math.pi * -0.20),
    "C3":   Quaternion(axis=[0,1,0], angle=math.pi * 0.20),
    "Pz":   Quaternion(axis=[1,0,0], angle=math.pi * -0.20),
    "Oz":   Quaternion(axis=[1,0,0], angle=math.pi * -0.40),
    "DLPFC": Quaternion(axis=[1,0,0], angle=math.pi * 0.25) * Quaternion(axis=[0,0,1], angle=math.pi/4) 
    }

markerWeights = {
    "FpZ":  0.5,
    "Fz":   0.75,
    "FCZ":  0.9,
    "Cz":   1,
    "C4":   0.75,
    "C3":   0.75,
    "Pz":   0.75,
    "Oz":   0.5,
}

# This function converts a marker into the quaternion required to
#  move this point to 0,0 (Cz)
#
# This does not take into account perspective and assumes
#  orthogonal projection..
def markerToQuaternion(marker, center, radius, forwardAngle):
    # Note there are some errors in this function that cause an
    #  abnormally large error when rotated beyond ~ +/-15 deg
    
    # Rotate the point about Z so that it's pointing down
    # (aligned with coordinate system)
    # The angle is manipulated to ensure it is in the correct range (an attempt to fix the error described above)
    forwardAngle = ((math.pi/2 - forwardAngle) + math.pi*2)%(math.pi*2)
    qYaw = Quaternion(axis=[0,0,1], angle=forwardAngle)
    
    x, y, r = marker

    # Get the offset to the  center of the head (assumed to be the
    #  center of the frame
    mP = [x - center[0], y - center[1], 0]

    # TODO FIgure out why this doesn't work great with different yaw..
    # Z coordinate is found with pythagoras, z2=r2-dxy2
    z2 = radius*radius - (mP[0]*mP[0] + mP[1]*mP[1])

    # Check that the Z2 is greater than 0, invalid points may have negative
    #  values here.
    if(z2 >= 0):

        # Since we know z2 is positive, calculate the sqrt()
        mP[2] = math.sqrt(z2)

        # rotate the point by yaw degrees
        mP = qYaw.rotate(mP)
        
        # mP is now facing down, get the pitch angle (dy)
        #  Approximation! since we assume the point is on a flat circle rather than on a ball...
        #  FudgeFactor compensates for this a bit.
        qPitch = Quaternion(axis=[1,0,0], angle=math.atan2(mP[1] * markerToQuaternionFudgeFactor, radius))

        # Rotate the point by pitch degrees
        mP = qPitch.rotate(mP)

        # mP is now facing down and aligned in the y axis, get the roll angle (dx)
        #  Approximation! since we assume the point is on a flat circle rather than on a ball...
        #  FudgeFactor compensates for this a bit.
        qRoll = Quaternion(axis=[0,-1,0], angle=math.atan2(mP[0] * markerToQuaternionFudgeFactor, radius))
        
        # The output quaternion that represents the rotation of the point to center.
        qTotal = qPitch * qRoll * qYaw

        return True, qTotal
    else:
        # print("Invalid!", z2)

        return False, None

# Ported from here: http://wiki.unity3d.com/index.php/Averaging_Quaternions_and_Vectors
#Get an average (mean) from more then two quaternions (with two, slerp would be used).
#Note: this only works if all the quaternions are relatively close together.
#Usage: 
#-Cumulative is an external Vector4 which holds all the added x y z and w components.
#-newRotation is the next rotation to be added to the average pool
#-firstRotation is the first quaternion of the array to be averaged
#-addAmount holds the total amount of quaternions which are currently added
#This function returns the current average quaternion
def averageQuaternion(cumulative, newRotation, firstRotation, addAmount, addWeight):
    w,x,y,z = 0,0,0,0

    # Before we add the new rotation to the average (mean), we have to check whether the quaternion has to be inverted. Because
    # q and -q are the same rotation, but cannot be averaged, we have to make sure they are all the same.
    if (not areQuaternionsClose(newRotation, firstRotation)):
        newRotation = newRotation.inverse

    # Average the values
    addDet = 1/float(addAmount);
    # W
    cumulative[0] += newRotation[0] * addWeight;
    w = cumulative[0] * addDet;

    # X
    cumulative[1] += newRotation[1] * addWeight;
    x = cumulative[1] * addDet;

    # Y
    cumulative[2] += newRotation[2] * addWeight;
    y = cumulative[2] * addDet;

    # Z
    cumulative[3] += newRotation[3] * addWeight;
    z = cumulative[3] * addDet;

    # note: if speed is an issue, you can skip the normalization step
    return Quaternion(w,x,y,z).normalised, cumulative
    
# Ported from here: http://wiki.unity3d.com/index.php/Averaging_Quaternions_and_Vectors
def areQuaternionsClose(q1, q2):
    if(np.dot(q1.elements, q2.elements)):
        return False
    return True
    
# This function takes various data from previous calculations to extrapolate the location of the target marker
#  This will also return the estimated position for any marker that can't be identified/seen!
#
# Returns array of markers [(x,y,r), name, 0:solved/1:extrapolated/2:unknown]
def processMarkers(markers, center, radius, forwardAngle, markerNames):
    markersOut = []
    knownMarkers = [] # list of markers that were identified
    unknownMarkers = [] # list of missing marker names

    # Quaternions that were solved
    solvedQ = []
    
    numOfMarkers = len(markers)
    
    for n in markerLocs:
        # for each marker we have a name/location for, check if it has been identified

        if(n in markerNames):
            # Marker known/found, get the index
            mIndex = markerNames.index(n)

            # Calculate the quaternion to move observed marker to center
            qOk, mQ = markerToQuaternion(markers[mIndex], center, radiusScaled, forwardAngle)

            if(qOk):
                # Conversion was successful!
                
                # Include the marker rotation relative to center/Cz
                #  This quaternion is the current markers *guess* for the head orientation
                mQ = mQ * markerLocs[n].inverse

                # Mark this marker as solved! and store its guess
                solvedQ.append([n, mQ])

                # Mark this marker as known
                knownMarkers.append(n)

                # Add this marker to the output array
                markersOut.append([markers[mIndex], n, 0])
            else:
                # Marker couldn't be solved, add it to the list of unknown markers
                unknownMarkers.append(n)
                
        else:
                # Marker couldn't be solved, add it to the list of unknown markers
            unknownMarkers.append(n)

    # Check that we didn't miss anything in markerNames
    #  This also adds markers that don't physically exist on the cap (including our target)
    for n in markerNames:
        if((not n in unknownMarkers) and (not n in knownMarkers)):
            unknownMarkers.append(n)

    # Check that at least 1 rotation was solved..
    if(len(solvedQ) > 0):
        # Average all rotations..
        addAmount = 0;
        addQ = Quaternion() #Identity, cumulative
        avgQ = None

        # Loop through each quaternion to take the average head angle..
        for q in solvedQ:
            addAmount = addAmount + markerWeights[q[0]]

            avgQ, addQ = averageQuaternion(addQ, q[1], solvedQ[0][1], addAmount, markerWeights[q[0]])
        
        centerVector = [0,0,radius]

        for um in unknownMarkers:
            # name of unknown marker ie. it isn't being tracked
            
            if(um in markerLocs):
                # If the unknown marker has a reference location, extrapolate it..
                mx,my,mz = (avgQ * markerLocs[um].inverse).rotate(centerVector)
                mx += center[0]
                my += center[1]
                
                # Get the marker radius
                mr = markerDefaultRadius
                if (um in markerNames):
                    mr = markers[markerNames.index(um)][2]

                markersOut.append([(mx,my,mr), um, 1])
            else:
                # Marker doesn't have a reference location, probably a visible but unknown point (or a guess)
                #  Just use the existing point/name
                # TODO: Find closest point and use that? Until then, just use the name/location

                mr = markerDefaultRadius
                if (um in markerNames):
                    mx,my,mr = markers[markerNames.index(um)]
                    
                    markersOut.append([(mx,my,mr), um, 2])
                    
    return markersOut
    
# This function takes a series of markers (coordinates) and tries
#  to figure out which 10-20coordinate/marker they are. Mostly does
#  this by making a few assumptions about the cap's geometry
def findMarkers(markers):
    markerNames = [str(s) for s in range(len(markers))]
    cZIndex = -1
    forwardAngle = 0
    if(len(markers) > 1):
        cZ = []
        
        result = []
        for i in range(len(markers)):
            cZ.append(0)
            result.append([])
        #for i in range(len(markers) - 1):
        for i in range(0, len(markers)-1):
            for j in range(i+1, len(markers)):
                dx = markers[j][0] - markers[i][0]
                dy = markers[j][1] - markers[i][1]

                ang = math.atan2(dy,dx)
                if(ang < 0):
                    ang = math.pi*2 + ang

                result[i].append(ang)
                result[j].append(ang)

        tooLinear = True
        for r in range(len(result)):
            minAng = min(result[r])
            for ang in result[r]:
                score = 0.5 - abs(((ang - minAng)*2/math.pi) % 1 - 0.5)
                if(score > 0.25):
                    tooLinear = False
                cZ[r] = cZ[r] + score

        # Check that the points aren't all on a line (something failed the test)
        if(tooLinear):
            # take point closest to top of screen..
            cZIndex = 0
            for m in range(1, len(markers)):
                if(markers[m][1] < markers[cZIndex][1]):
                    cZIndex = m

            markerNames[cZIndex] = "Cz"
            
            # Point down towards other points
            forwardAngle = (min(result[cZIndex]) + math.pi) % (math.pi*2)
        else:
            # Points aren't all in a line, we can figure out Cz..
            cZIndex = cZ.index(min(cZ))
            markerNames[cZIndex] = "Cz"

            # Since points aren't all in a line, we can check if we can find the forward marker (closest to cZ)
            # d = (index, distance, dx, dy, cross product)
            d = []
            minDist = -1
            for i in range(len(cZ)):
                if(i != cZIndex):
                    dx = markers[i][0] - markers[cZIndex][0]
                    dy = markers[i][1] - markers[cZIndex][1]
                    d.append([i, math.sqrt(dx*dx + dy*dy), dx, dy])
                    
            # Sort by distances
            d.sort(key=lambda tup: tup[1])

            # Check that smallest 2 points have a ratio less than fCZThreshold
            #  this means we have FcZ, since that point is closer than any other
            if(d[0][1]/d[1][1] < fCZThreshold):
                # First marker (closest to Cz) is FCZ
                markerNames[d[0][0]] = "FCZ"

                # Set the forward angle to be the angle to FCZ
                forwardAngle = math.atan2(d[0][3],d[0][2])#(min(result[cZIndex]) + math.pi) % (math.pi*2)

                # With FCZ and Cz we can assign names to pretty much everyother point

                for i in range(1, len(d)):
                    # Dot product is used to determine if the point is in a line
                    dp = (d[i][2]*d[0][2] + d[i][3]*d[0][3])/(d[i][1]*d[0][1])
                    if(abs(dp) >= hemiThreshold):
                        # Along the longitudinal fissure..

                        # check sign of determininent to determine if its in front/behind Cz
                        #  Not FcZ x/y have been swapped to rotate the vector by 90d
                        det = (d[i][2]*d[0][2] - d[i][3]*d[0][3])
                        if(det < 0):
                            # Same side as FCz, likely FPz, or Fz
                            
                            # Check distance ratios again..
                            if(d[0][1]/d[i][1] >= ofpZThreshold):
                                # i is much further than FCz, likely FPz
                                markerNames[d[i][0]] = "FpZ"

                            else:
                                markerNames[d[i][0]] = "Fz"
                                
                        else:
                            # Different side as FCz, likely Pz or Oz
                            
                            # Check distance ratios again..
                            if(d[0][1]/d[i][1] >= ofpZThreshold):
                                # i is much further than FCz, likely Oz
                                markerNames[d[i][0]] = "Oz"

                            else:
                                markerNames[d[i][0]] = "Pz"
                            
                    else:
                        # On a hemisphere, check the sign of determinant to determine which side
                        det = (d[i][2]*d[0][3] - d[i][3]*d[0][2])
                        if((det > 0) == leftHemiPositive):
                            # Left Hemisphere, C4
                            markerNames[d[i][0]] = "C4"
                        else:
                            # Right Hemisphere, C3
                            markerNames[d[i][0]] = "C3"
            else:
                # Cannot find FCZ, other points are pretty close to equally far away
                # Assume lowest point is Fz, and calculate C3/C4 based on cross product
                lowestPoint = 0
                for i in range(1, len(d)):
                    if(d[i][3] > d[lowestPoint][3]):
                        lowestPoint = i
                markerNames[d[lowestPoint][0]] = "Fz"

                for i in range(1, len(d)):
                    if(i != lowestPoint):
                        det = (d[i][2]*d[0][1] - d[i][1]*d[0][2])
                        if((det > 0) == leftHemiPositive):
                            # Left Hemisphere, C4
                            markerNames[d[i][0]] = "C4"
                        else:
                            # Right Hemisphere, C3
                            markerNames[d[i][0]] = "C3"
                            
                # the forward angle is assumed to be towards Fz (not very accurate), since we're on a ball
                forwardAngle =math.atan2(d[lowestPoint][3],d[lowestPoint][2])
            
    return cZIndex, forwardAngle, markerNames


lp.open(COMPort)


# font 
font = cv2.FONT_HERSHEY_SIMPLEX 

# Generate a circular mask that leaves 25px on the sides (for green mask)
maskCircle = np.zeros((croppedSize, croppedSize), np.uint8)
cv2.circle(maskCircle, (int(croppedSize/2), int(croppedSize/2)), int((croppedSize-50)/2), 255, thickness=-1)
                
vs = VideoStream(src=cameraIndex).start()

time.sleep(1)

lp.setLaser(0)
laserOn = False

frame = vs.read()
frame = imutils.resize(frame, width=frameWidth)
frameHeight, frameWidth, frameDepth = frame.shape

cameraDegPerPx = cameraFOV / math.sqrt(frameHeight * frameHeight + frameWidth * frameWidth)

lastLaserUpdateTime = time.time()

while(True):
    # Capture frame-by-frame
    frame = vs.read()
    frame = cv2.flip(frame, 1)
    cropped = None
    targetScreen = None
    
    # Find a green circle in the frame
    #  Adapted from www.pyimagesearch.com/2015/09/14/ball-tracking-with-opencv/
    
    # A. Resize image to reduce computational strain
    # B. Blur the image to remove high-frequency noise
    # C. Convert to hsv colour space
    frame = imutils.resize(frame, width=frameWidth)
    blurred = cv2.GaussianBlur(frame, (11, 11), 0)
    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

    # A. Mask the image to find the green pixels
    # B. Erode/Dilate the mask to remove small areas
    mask = cv2.inRange(hsv, greenLower, greenUpper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)

    # Find contours
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    center = None

    # If a green countour was found..
    if ( len(cnts) > 0 ):
        c = max(cnts, key=cv2.contourArea)
        ((x, y), radius) = cv2.minEnclosingCircle(c)
        
        # Calculate the moment/center of area
        M=cv2.moments(c)
        center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
        
        # Check that it is larger than the minimum radius
        if ( radius > minimumRadius ):

            # Calculate the scale when converting to the cropped image
            scale = radius / ((croppedSize + 20) / 2)
            radiusScaled = radius/scale

            # Get the x/y bounds of the cropped image
            x1 = int(x - radius - (10 * scale))
            x2 = int(x + radius + (10 * scale))
            y1 = int(y - radius - (10 * scale))
            y2 = int(y + radius + (10 * scale))
            
            # Set the cropped image used to process the markers..
            if(x1 >= 0 and x2 < frameWidth and y1 >= 0 and y2 < frameHeight):
                cropped = frame[y1:y2, x1:x2, :3]

                # Resize image
                #  This ensures that the processing happens on a similar sized object at all distances.
                cropped = cv2.resize(cropped, (croppedSize, croppedSize))
                xScale = (x2-x1)/croppedSize
                yScale = (y2-y1)/croppedSize

                # Generate a mask where there is green (to remove background)
                # We dilate/erode unevenly to ensure that we overmask the balloon a bit (and remove small
                #  green bits)
                maskGreen = cv2.dilate(mask, None, iterations=4)
                maskGreen = cv2.erode(maskGreen, None, iterations=10)
                maskGreen = cv2.resize(maskGreen[y1:y2, x1:x2], (croppedSize, croppedSize))

                # Or with a circle to ensure the center of the balloon is kept in
                #  When close the markers could be big enough to remain after erode/dilate
                maskGreen = cv2.bitwise_or(maskGreen, maskCircle)
                
                # Find markers/dots
                markers = []

                blurred2 = cv2.GaussianBlur(cropped, (9, 9), 0)
                hsv2 = cv2.cvtColor(blurred2, cv2.COLOR_BGR2HSV)

                # Find black markers..
                maskMarkers = cv2.inRange(hsv2, markerLower, markerUpper)
                maskMarkers = cv2.erode(maskMarkers, None, iterations=6)
                maskMarkers = cv2.dilate(maskMarkers, None, iterations=5)

                maskMarkers = cv2.bitwise_and(maskMarkers, maskGreen)

                cntsMarkers = cv2.findContours(maskMarkers.copy(),
                                               cv2.RETR_EXTERNAL,
                                               cv2.CHAIN_APPROX_SIMPLE)
                cntsMarkers = imutils.grab_contours(cntsMarkers)
                
                # Check that the markerCountours are bigger than the minimum radius
                for c in cntsMarkers:
                    ((mx, my), mradius) = cv2.minEnclosingCircle(c)
                    if(mradius > markerMinimumRadius):

                        markers.append((mx,my,mradius))

                # Use findMarkers to identify the observed markers..
                cZ, forwardAngle, markerNames = findMarkers(markers)

                # Process the markers to add in the missing/target markers..
                processedMarkers = processMarkers(markers, (croppedSize/2, croppedSize/2), radiusScaled, forwardAngle, markerNames)


                # Generate the information on the main image (circle markers, give markeer names, etc)
                for i in range(len(processedMarkers)):
                    [(mx, my, mradius), mname, solved] = processedMarkers[i]
                    fx = int(x1 + mx*xScale);
                    fy = int(y1 + my*yScale)
                    fr = int(mradius*xScale)
                    if(mname == "Cz"):
                        cv2.circle(frame, (fx, fy), fr, markerColourCz, 1)
                        cv2.line(frame, (fx, fy),
                                 (int(fx + math.cos(forwardAngle)*mradius), int(fy + math.sin(forwardAngle)*mradius)),
                                 markerColourCz, 1)
                        frame = cv2.putText(frame, mname, (fx, int(y1 + (my-mradius)*yScale)), font,
                                    0.5, markerColourCz, 1, cv2.LINE_AA)
                    else:
                        color = markerColourKnown
                        if(solved == 1): # Extrapolated
                            color = markerColourExtrapolate
                        elif(solved == 2): # Unknown
                            color = markerColourUnknown
                            
                        cv2.circle(frame, (fx, fy), fr, color, 2)
                        frame = cv2.putText(frame, mname, (fx, int(y1 + (my-mradius)*yScale)), font,
                                            0.5, color, 1, cv2.LINE_AA)

                    if(mname == targetMarkerName):
                        targetScreen = [fx,fy]
                        
            # Generate the circle around the cap
            cv2.circle(frame, (int(x), int(y)), int(radius), (0, 255, 255), 2)
            
            cv2.circle(frame, center, 3, (255, 0, 0), -1)
    # If a valid target is on the screen..
    if(targetScreen != None):
        
        # If it is time to update the servos..
        if(time.time() - lastLaserUpdateTime > laserUpdatePeriod):

            # Calculate the targetRoll/Pitches based on pixel position
            targetRoll = (targetScreen[0] - frameWidth/2) * cameraDegPerPx + laserOffset[1]
            targetPitch = (targetScreen[1] - frameHeight/2)  * cameraDegPerPx + laserOffset[0]

            # Command the laser designator..
            lp.setTarget(targetPitch, targetRoll)
            
            lastLaserUpdateTime = time.time()

        # If we have to turn the laser on, turn it on!
        if(laserOn == False):
            lp.setLaser(laserPower)
            laserOn = True
            
    elif(laserOn):
        # No target, turn the laser off!
        lp.setLaser(0)
        laserOn = False
    
    # Display the resulting frame
    cv2.imshow('frame', frame)
    if(not (cropped is None)):
        cv2.imshow('cropped', cropped)
        cv2.imshow('markers', maskMarkers)
        #cv2.imshow('mask', mask)

    # Wait until the 'q' key is pressed, stop the loop
    if(cv2.waitKey(1) & 0xFF == ord('q')):
        break
    
# END OF PROGRAM!

# Turn off the laser
lp.setLaser(0)
laserOn = False
        
    
# Release the capture device
vs.stop()
cv2.destroyAllWindows()

# Disconnect the serial port.
lp.close()
